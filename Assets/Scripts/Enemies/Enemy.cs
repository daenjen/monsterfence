﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [Header("Enemy Stats")]
    public int health = 10;
    public float speedMovement = 10f;
    public int moneyReward = 5;

    private Transform target;
    private int pathPointsIndex = 0;

    private void Start()
    {
        target = PathPoints.points[0];
    }

    private void Update()
    {
        Movement();
    }

    public void Movement()
    {
        Vector3 dir = target.position - transform.position;
        transform.LookAt(target);
        transform.Translate(dir.normalized * speedMovement * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.position) <= 0.4f)
        {
            GetNextPathPoints();
        }
    }

    public void GetNextPathPoints()
    {
        if (pathPointsIndex >= PathPoints.points.Length - 1)
        {
            //Check Lives and destroy monster         
            if (GameManager.Lives == 0)
            {
                Destroy(gameObject);
                return;
            }
            else
            {
                Destroy(gameObject);
                GameManager.Lives -= 1;
                return;
            }
        }

        pathPointsIndex++;
        target = PathPoints.points[pathPointsIndex];
    }

    public void TakeDamage(int amout)
    {
        health -= amout;
        if (health <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        Destroy(gameObject);
        GameManager.Money += moneyReward;
    }
}
