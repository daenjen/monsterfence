﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraController : MonoBehaviour
{
    public Transform lookAt;

    public float bottomScreen = -11f;
    public float leftScreen = -11f;

    public float smoothSpeed = 7.5f;
    public float distance = 5f;
    public float yOffSet = 3.5f;
    public float swipeRes = 200f;

    //locals
    [HideInInspector]public int rotationCount = 0;
    private Vector2 touchPos;
    private Vector3 desirePos;
    private Vector3 offSet;

    private void Start()
    {
        offSet = new Vector3(0, yOffSet, -1f * distance);
    }

    private void Update()
    {
        CameraKeyboardInputs();
        CameraJoystickInputs();
        CheckCameraRotationCount();  
    }

    private void FixedUpdate()
    {
        desirePos = lookAt.position + offSet;
        transform.position = Vector3.Lerp(transform.position, desirePos, smoothSpeed * Time.deltaTime);
        transform.LookAt(lookAt.position + Vector3.up);
    }

    private void CheckCameraRotationCount()
    {
        if (rotationCount == 4)
        {
            rotationCount = 0;
        }
        else if (rotationCount == -1)
        {
            rotationCount = 3;
        }
    }

    public void SlideCamera(bool left)
    {
        //keyboard swipe
        if (left)
        {
            offSet = Quaternion.Euler(0, 90, 0) * offSet;
            rotationCount++;
        }
        else
        {
            offSet = Quaternion.Euler(0, -90, 0) * offSet;
            rotationCount--;
        }
    }

    public void CameraKeyboardInputs()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            SlideCamera(true);
        else if (Input.GetKeyDown(KeyCode.RightArrow))
            SlideCamera(false);
    }

    public void CameraJoystickInputs()
    {
        //Debug.Log(touchPos);
        //if touch press
        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)) //if left or right mouse click
        {
            touchPos = Input.mousePosition;
        }

        if (touchPos.y > 300f && touchPos.x > 300f)
        {
            //if touch release)   
            if (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1)) //if left or right mouse click
            {
                float swipeForce = touchPos.x - Input.mousePosition.x;

                if (Mathf.Abs(swipeForce) > swipeRes)
                {
                    //swiping towards the left
                    if (swipeForce < 0)
                        SlideCamera(true);
                    //swiping towards the right
                    else if (swipeForce > 0)
                        SlideCamera(false);
                }
            }
        }
    }
}
