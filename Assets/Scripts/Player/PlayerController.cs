﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed = 10f;
    public float rotationSpeed = 25f;
    public float drag = 0.5f;

    public JoystickController joystick;

    //locals
    protected Rigidbody rb;

    private Transform camTrasform;
    Vector3 dir;
    Vector3 rotateDir;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.maxAngularVelocity = rotationSpeed;
        rb.drag = drag;
        camTrasform = Camera.main.transform;
    }

    private void FixedUpdate()
    {
        Movement();
    }

    void Update()
    { 
        Rotation();
    }

    public void Movement()
    {
        //Keyboard inputs
        dir = Vector3.zero;

        dir.x = Input.GetAxis("Horizontal");
        dir.z = Input.GetAxis("Vertical");

        if(dir.magnitude > 1)
        {
            dir.Normalize();
        }

        //Joystick inputs
        if (joystick.inputDirection != Vector3.zero)
        {
            dir = joystick.inputDirection;
        }

        var v3 = rotateDir * moveSpeed;
        v3.y = rb.velocity.y;
        rb.velocity = v3;
    }

    public void Rotation()
    {
        rotateDir = camTrasform.TransformDirection(dir);
        rotateDir = new Vector3(rotateDir.x, 0, rotateDir.z);
        rotateDir = rotateDir.normalized * dir.magnitude;
    }
}
