﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public class TowerInfo
//{
//    public GameObject towerObject;
//    public FirstTower tower;
//    //public Vector3 position { set; get; }
//    //public bool isOccupied { set; get; }
//}

public class BuildManager : MonoBehaviour
{
    public GameManager gameManager;

    //public List<GameObject> towerPrefabs;
    [Header("Towers")]
    public GameObject[] towers;

    [Header("Towers Name UI")]
    public GameObject LittleDragonPanelName;
    public GameObject GatlingPanelName;
    public GameObject FlamerPanelName;
    public GameObject SniperPanelName;

    [Header("Booleans")]
    public bool isActive = false;
    public bool canBuild = false;

    [Header("BuildMode Controls")]
    public float previewDistance = 3f;
    public float radius = 2f;
    public float maxDistance = 4f;

    //locals
    //private List<TowerInfo> towerSpawns;
    [SerializeField] private int towerIndex = 1;
    //private TowerInfo focusedTower;
    private Transform playerTransform;
    private Transform cameraTransform;
    private GameObject spawnPreview;
    private GameObject towerObject;
    private Collider[] hitColliders;
    private SkinnedMeshRenderer[] render;
    private Tower tower;
    private Vector2 touchPos;

    private void Awake()
    {
        spawnPreview = Instantiate(towers[towerIndex]) as GameObject;
        spawnPreview.GetComponent<Tower>().enabled = false;
        spawnPreview.GetComponent<Collider>().enabled = false;
        spawnPreview.gameObject.SetActive(false);
    }

    private void Start()
    {
        //towerSpawns = new List<TowerInfo>();      
        render = spawnPreview.GetComponentsInChildren<SkinnedMeshRenderer>();
        towerObject = towers[towerIndex];
        //tower = towers[towerIndex].GetComponent<Tower>();
    }

    private void Update()
    {
        if (!isActive)
            return;

        MovePrefabPreview();
    }

    public void InteractInput()
    {    
        if (isActive)
        {
            SpawnTower();
        }
    }

    public void BuildModeOpen()
    {
        if (isActive)
        {
            DisableBuildMode();
        }
        else
            ActivateBuildMode();
    }

    public void OnClickLeftArrow()
    {
        if (isActive)
        {
            if (towerIndex > 0)
            {
                towerIndex--;
                OnSelectionChange();
            }
        }
    }

    public void OnClickRightArrow()
    {
        if (isActive)
        {
            if (towerIndex < towers.Length - 1)
            {
                towerIndex++;
                OnSelectionChange();
            }
        }
    }

    public void OnSelectionChange()
    {
        //destroy the selected tower
        Destroy(spawnPreview);

        //create the next selected tower and removes components
        spawnPreview = Instantiate(towers[towerIndex] as GameObject);

        //if position [0] get...
        spawnPreview.GetComponent<Tower>().enabled = false;
        spawnPreview.GetComponent<Collider>().enabled = false;
        render = spawnPreview.GetComponentsInChildren<SkinnedMeshRenderer>();

        //check panel
        CheckPanelName();
    }

    public void ActivateBuildMode()
    {
        isActive = true;

        spawnPreview.gameObject.SetActive(true);

        //check panel
        CheckPanelName();
    }

    public void DisableBuildMode()
    {
        isActive = false;

        spawnPreview.gameObject.SetActive(false);
        LittleDragonPanelName.SetActive(false);
        GatlingPanelName.SetActive(false);
        FlamerPanelName.SetActive(false);
        SniperPanelName.SetActive(false);
    }

    public void MovePrefabPreview()
    {
        if (playerTransform == null || cameraTransform == null)
        {
            playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
            cameraTransform = Camera.main.transform;
            return;
        }

        //set preview position
        Vector3 previewPos = playerTransform.transform.position;
        previewPos += (playerTransform.transform.position - cameraTransform.transform.position).normalized * previewDistance;

        //check the ground for the preview
        RaycastHit hit;
        if (Physics.Raycast(previewPos + Vector3.up * 5, Vector3.down, out hit, 10f, LayerMask.GetMask("Ground")))
        {
            previewPos.y = hit.point.y + 0.2f;
        }

        hitColliders = Physics.OverlapSphere(spawnPreview.transform.position, radius);

        #region SetColorIfCanBuild
        canBuild = true;

        foreach (var col in hitColliders)
        {
            if (col.gameObject.tag != "Ground" && col.gameObject.tag != "Player" && col.gameObject.tag != "RaiseZone")
            {
                canBuild = false;
                break;
            }
        }

        if (canBuild)
        {
            for (int i = 0; i < render.Length; i++)
            {
                render[i].material.color = Color.green;
            }
        }
        else
        {
            for (int i = 0; i < render.Length; i++)
            {
                render[i].material.color = Color.red;
            }
        }
        #endregion

        spawnPreview.transform.position = previewPos;
    }

    public void SpawnTower()
    {
        if (canBuild)
        {
            tower = towers[towerIndex].GetComponent<Tower>();

            //check build cost
            if (GameManager.Money < tower.towerCost)
            {
                Debug.Log("Not enough money!");
                return;
            }

            //instantiate tower
            towerObject = Instantiate(towers[towerIndex], spawnPreview.transform.position, Quaternion.identity);

            //update currency in game
            GameManager.Money -= tower.towerCost;
        }
        else
            Debug.Log("Cannot Build!");
    }

    public void CheckPanelName()
    {
        if (towerIndex == 0)
        {
            LittleDragonPanelName.SetActive(true);
            GatlingPanelName.SetActive(false);
            FlamerPanelName.SetActive(false);
            SniperPanelName.SetActive(false);
        }
        if (towerIndex == 1)
        {
            LittleDragonPanelName.SetActive(false);
            GatlingPanelName.SetActive(true);
            FlamerPanelName.SetActive(false);
            SniperPanelName.SetActive(false);
        }
        if (towerIndex == 2)
        {
            LittleDragonPanelName.SetActive(false);
            GatlingPanelName.SetActive(false);
            FlamerPanelName.SetActive(true);
            SniperPanelName.SetActive(false);
        }
        if (towerIndex == 3)
        {
            LittleDragonPanelName.SetActive(false);
            GatlingPanelName.SetActive(false);
            FlamerPanelName.SetActive(false);
            SniperPanelName.SetActive(true);
        }
    }
}
