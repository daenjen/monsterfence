﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{ 
        [Header("Panels")]
        public GameObject mainMenu;

        public void Play()
        {
            SceneManager.LoadScene("build test");
        }

        public void QuitGame()
        {
            Application.Quit();
        }  
}
