﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public GameObject[] arrows;

    //locals
    private float arrowTimer = 0f;

    void Update()
    {
        ArrowAnimation();
    }

    public void ArrowAnimation()
    {
        if (GameManager.instance.open == true)
        {
            arrowTimer += Time.deltaTime;

            //switch Arrow
            if (arrowTimer > 1f)
            {
                arrows[0].gameObject.SetActive(false);
                arrows[1].gameObject.SetActive(true);
                arrows[2].gameObject.SetActive(false);
                arrows[3].gameObject.SetActive(true);
            }
            if (arrowTimer >= 2f)
            {
                arrows[0].gameObject.SetActive(true);
                arrows[1].gameObject.SetActive(false);
                arrows[2].gameObject.SetActive(true);
                arrows[3].gameObject.SetActive(false);
                arrowTimer = 0f;
            }
        }
        else
            arrowTimer = 0f;
    }
}

