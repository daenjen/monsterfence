﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Wave
{
    public GameObject enemy1;
    public int countEnemy1;
    public GameObject enemy2;
    public int countEnemy2;
    public GameObject enemy3;
    public int countEnemy3;
    public GameObject enemy4;
    public int countEnemy4;
    public float rate;
}

public class SpawnManager : MonoBehaviour
{
    //public static int enemiesAlive = 0;

    public Wave[] waves;

    public Transform spawnPoint;

    public float timeBetweenWaves = 5f;

    public Text waveCountDownText;
    //public List<Enemy> enemyPool = new List<Enemy>();

    //locals
    public static int waveIndex = 0;
    private float countDown = 2f;
    public GameManager manager;

    void Update()
    {
        if (countDown <= 0f)
        {
            StartCoroutine(SpawnWave());
            countDown = timeBetweenWaves;
        }

        countDown -= Time.deltaTime;

        waveCountDownText.text = Mathf.Round(countDown).ToString();
    }

    IEnumerator SpawnWave()
    {
        Wave wave = waves[waveIndex];

        //spawn enemy 1
        for (int i = 0; i < wave.countEnemy1; i++)
        {
            SpawnEnemy(wave.enemy1);
            yield return new WaitForSeconds(1f / wave.rate);
        }
        //spawn enemy 2
        for (int i = 0; i < wave.countEnemy2; i++)
        {
            SpawnEnemy(wave.enemy2);
            yield return new WaitForSeconds(1f / wave.rate);
        }
        //spawn enemy 3
        for (int i = 0; i < wave.countEnemy3; i++)
        {
            SpawnEnemy(wave.enemy3);
            yield return new WaitForSeconds(1f / wave.rate);
        }
        //spawn enemy 4
        for (int i = 0; i < wave.countEnemy4; i++)
        {
            SpawnEnemy(wave.enemy4);
            yield return new WaitForSeconds(1f / wave.rate);
        }

        waveIndex++;
        //check last wave
        if(waveIndex == waves.Length)
        {
            manager.levelComplete.SetActive(true);

            this.enabled = false;
        }
    }

    public void SpawnEnemy(GameObject enemy)
    {
        Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
        //enemiesAlive++;
    }
}
