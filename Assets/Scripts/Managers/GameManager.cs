﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [Header("Stats")]
    public static int Lives;
    public int actualLives = 5;

    [Header("Currency")]
    public static int Money;
    public int actualMoney = 200;

    [Header("UI")]
    public static CameraController Camera;
    public GameObject Inventory;
    public GameObject PanelBuildSelection;
    public Text textMoney;
    public Text textLives;
    public Text textSellAmount;
    public Text textWaveCount;

    [Header("Panels")]
    public GameObject mainMenu;
    public GameObject pausePanel;
    public GameObject gameOverUI;
    public GameObject levelComplete;
    public Text textGameOverCountDown;
    public float gameOverCountDown = 60;

    //locals
    public static bool gameEnded = false;
    public bool open = false;

    private void Awake()
    {
        instance = this;

        if (Camera == null)
            Camera = FindObjectOfType<CameraController>();
    }

    void Start()
    {
        Lives = actualLives;
        Money = actualMoney;
    }

    void Update()
    {
        textMoney.text = "§" + Money.ToString();

        textLives.text = "♥" + Lives.ToString();

        textWaveCount.text = "Wave:" + SpawnManager.waveIndex.ToString();

        //if (Lives <= 0)
        //{
        //    Lives = 0;
        //}

        if (gameEnded == true)
        {
            gameOverCountDown -= Time.deltaTime;
            textGameOverCountDown.text = Mathf.Round(gameOverCountDown).ToString();
            textWaveCount.gameObject.SetActive(false);
        }

        GameOver();

        OpenPauseMenu();
    }
    public void Play()
    {
        SceneManager.LoadScene("build test");
    }

    public void OpenInventory()
    {
        if (open == false)
        {
            PanelBuildSelection.SetActive(true);
            open = true;
        }
        else if (open == true)
        {
            PanelBuildSelection.SetActive(false);
            open = false;
        }
    }

    public void OpenPauseMenu() //When i press Esc, pause the game and open PauseMenu
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pausePanel.SetActive(true);
            Time.timeScale = 0;
        }
    }

    public void ResumePauseMenu()   //When i press resume, close the PauseMenu and unpause the game
    {
        pausePanel.SetActive(false);
        Time.timeScale = 1;
    }

    public void RestartGame()   //When i press restart, reload scene MainMenu
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        SceneManager.LoadScene("build test");
        Time.timeScale = 1;
        SpawnManager.waveIndex = 0;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void BacktoMainMenu()
    {
        SceneManager.LoadScene("Main Menu");
        Time.timeScale = 1;
    }

    public void GameOver()
    {
        if (gameEnded)
            return;

        if (Lives <= 0)
        {
            EndGame();
        }
    }

    public void EndGame()
    {
        gameEnded = true;

        //if game over, close build panel
        if (gameEnded)
        {
            PanelBuildSelection.SetActive(false);
            gameOverUI.SetActive(true);
        }

        //Time.timeScale = 0;

        if (gameOverCountDown <= 0)
        {
            SceneManager.LoadScene("Main Menu");
        }
    }
}
