﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public Transform target;
    public Transform partToRotate;
    public Transform muzzleTower;
    public GameObject towerUI;

    [Header("Tower Stats")]
    public int towerCost;

    [Header("Tower Upgrade")]
    public GameObject upgradePrefab;
    public int upgradeCost;

    //[Header("Effects")]
    //public GameObject buildEffect;
    //public GameObject sellEffect;

    [Header("Shoot stats")]
    public GameObject bulletPrefab;
    public int damage = 5;
    public float range = 15f;
    public float fireRate = 1f;
    public float turnSpeed = 10f;
    private float fireCountDown = 0f;

    public string enemyTag = "Enemy";

    Vector3 towerPos;

    public void GetTarget()
    {
        //find all enemy with enemyTag
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortDistance)
            {
                //get the enemy
                shortDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }

            //if we found and enemy in our range
            if (nearestEnemy != null)
            {
                //set the target
                if (shortDistance <= range)
                {
                    target = nearestEnemy.transform;
                }
                else
                    target = null;
            }
        }
    }

    public void Rotation()
    {
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);

        //smooth the rotation on target lock
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation,
            lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    public void ShootTarget()
    {
        if (fireCountDown <= 0f)
        {
            Shoot();
            fireCountDown = 1f / fireRate;
        }

        fireCountDown -= Time.deltaTime;
    }

    public void Shoot()
    {
        GameObject bullet1 = (GameObject)Instantiate(bulletPrefab, muzzleTower.transform.position, muzzleTower.rotation);
        Bullet bullet = bullet1.GetComponent<Bullet>();
        bullet.damage = this.damage;

        if (bullet != null)
        {
            bullet.Seek(target);
        }
    }

    public void UpgradeTower()
    {
        if (upgradePrefab == null)
            return;

        if (GameManager.Money < upgradeCost)
        {
            Debug.Log("Not enough money to upgrade!");
            return;
        }

        towerPos = transform.position;
        //Destroy old tower
        Destroy(gameObject);

        //Replace the old tower with the new one
        Instantiate(upgradePrefab, towerPos, Quaternion.identity);
        GameManager.Money -= upgradeCost;

        Debug.Log("Upgrade complete!");
    }

    public void SellTower()
    {
        GameManager.Money += GetSellAmount();
        Destroy(gameObject);

        //Sell effect
        //GameObject effect = (GameObject)Instantiate(sellEffect, towerPos, Quaternion.identity);
        //Destroy(sellEffect, 5f);

    }

    #region Utilities
    public int GetSellAmount()
    {
        return towerCost / 2;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
    #endregion
}
