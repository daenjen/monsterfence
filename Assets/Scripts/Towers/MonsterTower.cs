﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterTower : Tower
{
    private Vector3 touchPos;

    public bool canClick = false;
    public bool moreRange = false;
    public bool check = false;
    public float increaseRange = 10f;

    void Start()
    {
        InvokeRepeating("GetTarget", 0f, 0.5f);
    }

    void Update()
    {
        UIOnClick();

        AddRange();

        if (target == null)
            return;

        Rotation();
        ShootTarget();

        touchPos = Input.mousePosition;
    }

    private void UIOnClick()
    {
        if (canClick)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (GameManager.Camera.rotationCount == 0)
                {
                    towerUI.SetActive(true);
                    towerUI.transform.eulerAngles = new Vector3(0, 0, 0);
                }
                else if (GameManager.Camera.rotationCount == 1)
                {
                    towerUI.SetActive(true);
                    towerUI.transform.eulerAngles = new Vector3(0, 90, 0);
                }
                else if (GameManager.Camera.rotationCount == 2)
                {
                    towerUI.SetActive(true);
                    towerUI.transform.eulerAngles = new Vector3(0, 180, 0);
                }
                else if (GameManager.Camera.rotationCount == 3)
                {
                    towerUI.SetActive(true);
                    towerUI.transform.eulerAngles = new Vector3(0, 270, 0);
                }
            }
        }
        else
            towerUI.SetActive(false);
    }

    public void AddRange()
    {
        if (moreRange == true && check == false)
        {
            range += increaseRange;
            check = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
            canClick = true;
        if (other.tag == "RaiseZone")
            moreRange = true;
    }

    private void OnTriggerExit(Collider other)
    {
        canClick = false;
    }
}

