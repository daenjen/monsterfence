﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class JoystickController : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    public float timeUsed = 2f;
    private float time = 0f;

    private Image bgImg;
    private Image joystickImg;

    public Vector3 inputDirection { set; get; }
    public Vector2 pos;

    private void Start()
    {
        bgImg = GetComponent<Image>();
        joystickImg = transform.GetChild(0).GetComponent<Image>();
        inputDirection = Vector3.zero;
        pos = Vector2.zero;
    }

    public virtual void OnDrag(PointerEventData eventData)
    {
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle
            (bgImg.rectTransform,
            eventData.position,
            eventData.pressEventCamera, out pos))
        {
            pos.x = (pos.x / bgImg.rectTransform.sizeDelta.x);  //we get the position click in the rectangle x
            pos.y = (pos.y / bgImg.rectTransform.sizeDelta.y);  //we get the position click in the rectangle y

            float x = (bgImg.rectTransform.pivot.x == 1) ? pos.x * 2+1  : pos.x * 2 /*-1*/;   //get the pivot and multiply in direction of pivot position 
            float y = (bgImg.rectTransform.pivot.y == 1) ? pos.y * 2 +1 : pos.y * 2 /*-1*/;

            inputDirection = new Vector3(x, 0, y);  //convert vector2 in vector3 ignoring y
            inputDirection = (inputDirection.magnitude > 1) ? inputDirection.normalized : inputDirection;   //limit joystick to background image
                
            //move the joystick image
            joystickImg.rectTransform.anchoredPosition = new Vector3(inputDirection.x *
                (bgImg.rectTransform.sizeDelta.x / 3),
                inputDirection.z * (bgImg.rectTransform.sizeDelta.y / 3));
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        OnDrag(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        inputDirection = Vector3.zero;
        joystickImg.rectTransform.anchoredPosition = Vector3.zero;
    }
}
