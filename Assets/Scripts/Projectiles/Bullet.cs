﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Transform target;

    public GameObject BulletImpactEffect;

    public int damage;
    public float speed = 50f;

    public void Seek(Transform _target)
    {
        target = _target;
    }

    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dirToTarget = target.position - transform.position;
        float distance = speed * Time.deltaTime;

        if (dirToTarget.magnitude <= distance)
        {
            HitTarget();
            return;
        }

        transform.Translate(dirToTarget.normalized * distance, Space.World);
    }

    public void HitTarget()
    {
        GameObject effect = Instantiate(BulletImpactEffect, transform.position, transform.rotation);
        Destroy(effect, 2f);

        Damage(target);
        Destroy(gameObject);
    }

    public void Damage(Transform enemy)
    {
        Enemy e = enemy.GetComponent<Enemy>();
        if (e != null)
        {
            e.TakeDamage(damage);
        }
    }
}
